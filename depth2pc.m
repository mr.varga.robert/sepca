function pts = depth2pc(depth)
% depth map to 3d point cloud
h = size(depth,1);
w = size(depth,2);
z = double(depth(:));
[x, y] = meshgrid(1:w, 1:h);
x = x(:);
y = y(:);
x = x - w/2;
y = y - h/2;
% x = x ./ (w/2);
% y = y ./ (w/2);
% x = x .* z;
% y = y .* z;
% pts = [z'; x'; y'];
f = w/2;
r = z ./ sqrt(x.^2 + y.^2 + f^2);
x = r .* x;
y = r .* y;
z = r .* f;
pts = [z'; x'; y'];
end