function [em, pt_count] = em_addpts(pts, params, em, pt_count)
uv = world2grid(pts, params);

uv = floor(uv);
ind = uv(1,:) >= 0 & uv(1,:) < params.height & ...
      uv(2,:) >= 0 & uv(2,:) < params.width;

pts = pts(:, ind);
uv = uv(:, ind) + 1;

for k = 1:size(pts,2)
    z = pts(3, k);
    z = 100 - z;
    em(uv(1,k), uv(2,k)) = max(z, em(uv(1,k), uv(2,k)));
    pt_count(uv(1,k), uv(2,k)) = pt_count(uv(1,k), uv(2,k))+1;
end