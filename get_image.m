function img = get_image(folder, traj, time, name)
change_name = 0;
if name == "depth_vlad", change_name = 1; name = 'depth'; end
s = sprintf('/trajectory_%04d/camera_data/%s', traj, name);
fnames = h5read(fullfile(folder, 'sensor_records.hdf5'), s);
fname = fnames(1 + floor(25*time));
if change_name, fname = strrep(fname, 'depth', 'depth_vlad'); end
% fprintf('loading image %s\n', fname);
fname = fullfile(folder, fname);
img = imread(fname);
if name == "depth"
    if change_name        
        img = 2*double(img) / 256;
        img = imresize(img, 2, 'bilinear');
    else
        img = short2fp16(img);    
    end
    
%     fname = char(fname);
%     fname(end-2:end) = "bin";
%     fid = fopen(fname, "rb");
%     img = fread(fid, 1024 * 1024, 'float32');
%     img = reshape(img, 1024, 1024)';    
    
%     sum(sum(abs(img-img0)))
%     img = img';
%     img = min(1250, img);
%     img = log(img-1)./(log(1250)-1);
end
end