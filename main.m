clear; clc; format compact; tic;

% folder = 'd:\midair\zzz_dl\MidAir\Kite_training\sunny\';
folder = 'd:\midair\zzz_dl\MidAir\PLE_training\fall\';
traj = 4000;
use_pcloud = 1;
t0 = 0*20*0.04; %start time in seconds
dt = 1*0.04; %difference between frames (multiple of 0.04)
eps = 1e-3; %add this to correct for floating point errors
T = 500; %no. of frames to combine

hdf5 = fullfile(folder, 'sensor_records.hdf5');
info = h5info(hdf5);

pos = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/position', traj));
rot = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/attitude', traj));

figure(1), clf, hold on;
grid on; xlabel('y'); ylabel('x'); zlabel('-z');
lims = [-1e3 100 -1e3 1e3 -1e3 1e3];

figure(4), clf;
title('Colored 3D point cloud'), hold on;
xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)'), grid on;

% writerObj = VideoWriter('colored_pcloud_PLE4000.avi');
% writerObj.FrameRate = 10;
% open(writerObj);

for i=1:T
    t = t0 + (i-1)*dt + eps;
    try
        left = get_image(folder, traj, t, 'color_left');
%         figure(2), imshow(left);
        depth = get_image(folder, traj, t, 'depth');
            figure(3), imagesc(single(depth));
    catch, break; end
    
    pts = depth2pc(depth);
    [pts, inds] = filterxyz(pts, lims);
    left = reshape(left, [1024*1024, 3]);
    colors = left(inds, :)';
    
    ptst = pts;
    
    ind = floor(100*t)+1; % 1-indexing in Matlab
    fprintf('loading gt data of index %d\n', ind);
    
    q = quaternion(rot(:, ind)');
    R = rotmat(q, 'point');
    T = pos(:, ind);
    ptst = R * ptst + T;
    
    if use_pcloud
        ptst(3,:) = -ptst(3,:);
        pc = pointCloud(ptst', 'Color', colors');
        pc_small = pcdownsample(pc, 'random', 0.01);
        figure(4), pcshow(pc_small);
        
        frame = getframe(gcf);
%         writeVideo(writerObj, frame);
        
%         im = frame2im(frame);
%         [imind,cm] = rgb2ind(im,256);
%         if i == 1
%             imwrite(imind, cm, 'out.gif', 'gif', 'Loopcount', inf);
%         else
%             imwrite(imind, cm, 'out.gif', 'WriteMode','append','DelayTime',0.1);
%         end        
    else
        %         ptst = ptst(:, 1:100:end);
        figure(1),
        plot3(ptst(2, :), ptst(1, :), -ptst(3, :), 'k.');
    end
end
% close(writerObj);