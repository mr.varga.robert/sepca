clear; clc; format compact; tic;

% folder = 'd:\midair\zzz_dl\MidAir\Kite_training\sunny\';
folder = 'd:\midair\zzz_dl\MidAir\PLE_training\fall\';
traj = 4000;
use_pcloud = 1;
t0 = 0*20*0.04; %start time in seconds
dt = 25*0.04; %difference between frames (multiple of 0.04)
eps = 1e-3; %add this to correct for floating point errors
T = 1000; %no. of frames to combine

hdf5 = fullfile(folder, 'sensor_records.hdf5');
info = h5info(hdf5);

pos = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/position', traj));
rot = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/attitude', traj));

% figure(1), clf, hold on;
% grid on; xlabel('y'); ylabel('x'); zlabel('-z');
% view([-80, 37]);
lims = [-1e3 1e3 -1e3 1e3 -1e3 1e3];

%elevation map params
% height, width, grid size u, grid size v
params = struct;
params.height = 1024;
params.width = 1024;
params.su = 0.5;
params.sv = 0.5;
em = -0*ones(params.height, params.width);
pt_count = em;

for i=1:T
    t = t0 + (i-1)*dt + eps;
    try
        left = get_image(folder, traj, t, 'color_left');
%         figure(2), imshow(left);
        depth = get_image(folder, traj, t, 'depth');
%             figure(3), imagesc(single(depth));
    catch, break; end
    
    pts = depth2pc(depth);
    [pts, inds] = filterxyz(pts, lims);
    left = reshape(left, [1024*1024, 3]);
    colors = left(inds, :)';
    
    ptst = pts;
    
    ind = floor(100*t)+1; % 1-indexing in Matlab
    fprintf('loading gt data of index %d\n', ind);
    
    q = quaternion(rot(:, ind)');
    R = rotmat(q, 'point');
    T = pos(:, ind);
    ptst = R * ptst + T;
    
    [em, pt_count] = em_addpts(ptst, params, em, pt_count);
    
    if mod(i-1, 10) == 0
        figure(2), imagesc(em), colormap(gray);
%         toc
%         figure(3), imagesc(pt_count)
    end
end
figure(2), imagesc(em), colormap(gray);
%         figure(3), imagesc(pt_count)

