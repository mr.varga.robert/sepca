clear; clc; format compact; tic;

% folder = 'd:\midair\zzz_dl\MidAir\Kite_training\sunny\';
folder = 'd:\midair\zzz_dl\MidAir\PLE_training\fall\';
traj = 4000;
use_pcloud = 1;
t0 = 0*20*0.04; %start time in seconds
dt = 10*0.04; %difference between frames (multiple of 0.04)
eps = 1e-3; %add this to correct for floating point errors
T = 1000; %no. of frames to combine

dmax = 100;
cnt_dist = zeros(1, 100);
error_dist = zeros(1, 100);
error_dist_abs = zeros(1, 100);
error_img = zeros(1024, 1024);

for i=1:T
    t = t0 + (i-1)*dt + eps;
    
    depth_gt = get_image(folder, traj, t, 'depth');
    depth = get_image(folder, traj, t, 'depth_vlad');
    depth = double(depth);
    depth_gt = double(depth_gt);
    
    diff = abs(depth_gt - depth);    
    diff_rel = diff ./ depth_gt;    
    diff(diff == Inf) = 100;
    
    for d = 1:100
        ind = round(depth_gt(:)) == d;
        cnt_dist(d) = cnt_dist(d) + sum(ind);
        error_dist(d) = error_dist(d) + sum(diff_rel(ind));
        error_dist_abs(d) = error_dist_abs(d) + sum(diff(ind));
    end
    
    error_img = error_img + double(diff);
    
end
error_img = error_img ./ T;

figure(1);
imagesc(error_img);
title('absolute error based on image region');

figure(2);
error_dist = error_dist ./ (cnt_dist+1);
bar(1:100, error_dist);
title('average relative error based on distance');
xlabel('distance'), ylabel('relative error');

figure(3);
error_dist_abs = error_dist_abs ./ (cnt_dist+1);
bar(1:100, error_dist_abs);
title('average absolute error based on distance');
xlabel('distance'), ylabel('absolute error');