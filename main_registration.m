clear; clc; format compact; tic;

% folder = 'd:\midair\zzz_dl\MidAir\Kite_training\sunny\';
folder = 'd:\midair\zzz_dl\MidAir\PLE_training\fall\';
traj = 4000;
use_pcloud = 1;
t0 = 0*20*0.04; %start time in seconds
dt = 25*0.04; %difference between frames (multiple of 0.04)
eps = 1e-3; %add this to correct for floating point errors
T = 500; %no. of frames to combine
% ds = 1e-2; %down-sample rate
% D = 0.5 * 1.5.^(-3 + 0:6);
D = [1e-1];

% registration method icp, ndt, cpd, em
method = 'ndt';
view_pcs = 0;

hdf5 = fullfile(folder, 'sensor_records.hdf5');
info = h5info(hdf5);

pos = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/position', traj));
rot = h5read(hdf5, sprintf('/trajectory_%04d/groundtruth/attitude', traj));

figure(1), clf, hold on;
grid on; xlabel('y'); ylabel('x'); zlabel('-z');
lims = [-1e3 100 -1e3 1e3 -1e3 1e3];

figure(4), clf;
title('Colored 3D point cloud'), hold on;
xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)'), grid on;

for ii = 1:length(D)
    pos_err = zeros(1, 1);
    ang_err = zeros(1, 3);
    cnt = 0;
    tic
    ds = D(ii);
    for i=1:T-1
        t1 = t0 + (i-1)*dt + eps;
        t2 = t0 + (i-0)*dt + eps;
        try
            left1 = get_image(folder, traj, t1, 'color_left');
            left2 = get_image(folder, traj, t2, 'color_left');
            depth1 = get_image(folder, traj, t1, 'depth');
            depth2 = get_image(folder, traj, t2, 'depth');
            %         figure(2), imshow(left);
            %         figure(3), imagesc(single(depth));
        catch, break; end
        
        pts1 = depth2pc(depth1);
        [pts1, inds1] = filterxyz(pts1, lims);
        left1 = reshape(left1, [1024*1024, 3]);
        colors1 = left1(inds1, :)';
        
        pts2 = depth2pc(depth2);
        [pts2, inds2] = filterxyz(pts2, lims);
        left2 = reshape(left2, [1024*1024, 3]);
        colors2 = left2(inds2, :)';
        
        ind1 = floor(100*t1)+1; % 1-indexing in Matlab
        ind2 = floor(100*t2)+1; % 1-indexing in Matlab
        %     fprintf('loading gt data of index %d\n', ind1);
        
        if 0
            % test if registration method can recover affine transf
            A = eye(4);
            A(1:3, 1:3) = eul2rotm([0, 0.1, 0]);     %rotation
            A(4, 1) = -1;   %translation
            pc1 = pointCloud(pts1', 'Color', colors1');
            pc1 = pcdownsample(pc1, 'random', 0.01);
            pc1t = pctransform(pc1, affine3d(A));
            T = pcregistericp(pc1, pc1t);
            %         T = pcregisterndt(pc1, pc1t, 1);
            %         T = pcregistercpd(pc1, pc1t, 'Transform', 'Rigid');
            err = abs(A-T.T);
            sum(err(:))
            break;
        end
        
        q = quaternion(rot(:, ind1)');
        R1 = rotmat(q, 'point');
        T1 = pos(:, ind1);
        %     ptst = R * ptst + T;
        
        q = quaternion(rot(:, ind2)');
        R2 = rotmat(q, 'point');
        T2 = pos(:, ind2);
        
        X10 = R1 * pts1 + T1;
        %     X10 = R2 * X102 + T2;
        X102 = R2' * ( X10 - T2 );
        
        T12 = R2' * (T1 - T2);
        R12 = R2' * R1;
        ang12 = rotm2eul(R12);
        
        X102s = R12 * pts1 + T12;
        
        pc1 = pointCloud(pts1', 'Color', colors1');
        pc2 = pointCloud(pts2', 'Color', colors2');
%         pc1s = pcdownsample(pc1, 'gridAverage', ds);
%         pc2s = pcdownsample(pc2, 'gridAverage', ds);
            pc1s = pcdownsample(pc1, 'random', ds);
            pc2s = pcdownsample(pc2, 'random', ds);
        
        if method == 'icp'
            Tr = pcregistericp(pc1s, pc2s, 'Metric', 'pointToPlane');
        elseif method == 'ndt'
            Tr = pcregisterndt(pc1s, pc2s, 1);
        elseif method == 'cpd'
            Tr = pcregistercpd(pc1s, pc2s, 'Transform', 'Rigid');
        end
        
        R12e = Tr.Rotation';
        ang12e = rotm2eul(R12e);
        T12e = Tr.Translation;
        
        pos_err = pos_err + abs(T12' - T12e);
        ang_err = ang_err + min([abs(ang12 - ang12e); ...
            abs(2*pi + ang12 - ang12e); ...
            abs(-2*pi + ang12 - ang12e)], [], 1);
        cnt = cnt+1;
        
        fprintf('%4d: pos err %.4f %.4f %.4f %.4f %.4f %.4f %.4f\n', ...
            cnt, pos_err(1)/cnt, pos_err(2)/cnt, pos_err(3)/cnt, ...
            ang_err(1)/cnt, ang_err(2)/cnt, ang_err(3)/cnt, ...
            (sum(pos_err) + sum(ang_err))/cnt);
        
        if view_pcs
            figure(4), clf, hold on
            Tgt = rigid3d(R12', T12');
            pc12_gt = pctransform( pc1 , Tgt );
            pc12_est = pctransform( pc1 , Tr );
            
            pcshow(pc2);
            %         pcshow(pc12_gt);
            pcshow(pc12_est);
        end
        
        if cnt == 100, break; end
    end
    toc / cnt
    errs(ii) = sum(pos_err);
end

figure(5), clf
plot(D, errs/cnt);
xlabel('grid step')
ylabel('translation error')
grid on;
title('ICP - point to plane');

