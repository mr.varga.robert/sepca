function pts = depth2pc(depth, params)
% depth map to 3d point cloud for SEPCA pcl_export
h = size(depth,1);
w = size(depth,2);
z = double(depth(:));
[x, y] = meshgrid(1:w, 1:h);
x = x(:);
y = y(:);
K = params.intrinsicMatrix;

x = (x - K(3, 1)) / K(1, 1);
y = (y - K(3, 2)) / K(2, 2);

% x = x - w/2;
% y = y - h/2;
% x = x ./ (w/2);
% y = y ./ (w/2);

x = x .* z;
y = y .* z;
pts = [z'; x'; y'];
end
