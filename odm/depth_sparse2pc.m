function [pts, idx] = depth_sparse2pc(X, params)
X = reshape(X, [], 4);
idx = sum(abs(X), 2) > 0;
X = X(idx, :);
pts = X(:, 1:3)';
