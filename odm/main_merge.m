clear; clc; format compact;
folder = 'd:\sepca\2020_10_02\sparse_export\';
folder_depth = 'd:\sepca\2020_10_02\sparse_export_depth_vlad\';
i0 = 1;
i1 = i0+50;
ss = 100;
nrc = 4;
use_gt = 0;

figure(1), clf, title('color image');

figure(4), clf;
title('Colored 3D point cloud'), hold on;
xlabel('X (m)'); ylabel('Y (m)'); zlabel('Z (m)'), grid on;

for i = i0:i1
    disp(i);
    try
    fname = sprintf('%s100_0013_%04d.jpg.json', folder, i);
    params = jsondecode(fileread(fname));    
    catch continue, end
    
    fname = sprintf('%s100_0013_%04d.jpg.png', folder, i);
    img = imread(fname);
    img = reshape(img, [params.height, params.width, 3]);    
    colors = reshape(permute(img, [2, 1, 3]), [], 3)';
%     figure(1), imshow(img);
    
    fname = sprintf('%s100_0013_%04d.jpg.bin', folder, i);    
    depth = read_bin(fname, params, 4);
    
    fname = sprintf('%s100_0013_%04d.jpg.png', folder_depth, i);    
    depth_vlad = imread(fname);
    
    err = abs(double(depth_vlad) - depth(:, :, 3));
%     err = mean(err(:))
%     figure(2), imagesc([depth(:, :, 3), depth_vlad]);
    
    if ~use_gt, depth(:, :, 3) = double(reshape(depth_vlad', [], size(depth, 2))); end
    
    if nrc == 1
        pts_cam = depth2pc(depth, params);  
    else
        [pts_cam, idx] = depth_sparse2pc(depth, params);
        colors = colors(:, idx);
    end
    pts_cam = pts_cam(:, 1:ss:end);
    colors = colors(:, 1:ss:end);
%     pts_world = pts_cam;
    pts_world = params.rotation * pts_cam  + params.translation * ones(1, size(pts_cam, 2));
%     pts_world = params.rotation * (pts_cam - params.translation * ones(1, size(pts_cam, 2)));
%     pts_world = params.rotation * pts_cam;
%     pts_world = params.rotation * pts_cam + params.translation * ones(1, size(pts_cam, 2));
    
%     pc = pointCloud(pts_world');
    pc = pointCloud(pts_world', 'Color', colors');

    figure(4), pcshow(pc);
    
%     imagesc(depth);
%     waitforbuttonpress;
end