function X = read_bin(fname, params, nrc)
f = fopen(fname);
if nrc == 1
    X = fread(f, inf, 'float64');
    X = reshape(X, [params.height, params.width]);
else
    X = fread(f, inf, 'float32');
    X = reshape(X, [params.height, params.width, nrc]);
%     X = permute(X, [2, 1, 3]);
end
    
