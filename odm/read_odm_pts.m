clear;
tic
file = 'd:\sepca\2020_10_02\maps_oblique\odm_georeferencing\odm_georeferenced_model.csv';
pts = csvread(file, 1, 0);
pc = pointCloud(pts);
pc_small = pcdownsample(pc, 'random', 1e-2);
figure(4), pcshow(pc_small);
toc