function Y = short2fp16(X)
% Half precision float: sign bit, 5 bits exponent, 10 bits mantissa
s = half(bitshift(X, -15));
e = half(bitand(bitshift(X, -10), 31) - 15);
m = half(bitand(X, 1023));
Y = (-1).^s .* (1 + m.*2^(-10)) .* 2.^e;   