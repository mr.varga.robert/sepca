function uv = world2grid(xyz, params)
uv = xyz(1:2, :);
uv(1, :) = uv(1, :) ./ params.su;
uv(2, :) = uv(2, :) ./ params.sv;
uv(1, :) = params.height - uv(1, :);
uv(2, :) = params.width/2 + uv(2, :);
